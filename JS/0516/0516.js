$(fun1);

function fun1(){
    $("#d1>div").text("我的选择器练习");
    $("#d1>ul>li:even").css({"font-size":"30px","background-color":"red"});
    $("li>a:contains('综合')").click(fun2);
    $("li>a:contains('其他')").click(fun3);
    $("li>a:contains('子元素')").mouseover(fun4);
}

function fun2(){
    if($("a:contains('综合')").text()=="综合例子，隐藏"){
        // $("#d1>ul>li:odd").hide();
        // $("#d1>ul>li:odd").addClass("my");
        $("#d1>ul>li:odd").css("display","none");
        $("a:contains('综合')").text("综合例子，显示");
    }
    else{
        // $("#d1>ul>li:hidden").show();
        // $("#d1>ul>li:hidden").removeClass("my");
        $("#d1>ul>li:hidden").css("display","block");
        $("a:contains('综合')").text("综合例子，隐藏");
    }
}

function fun3(){
    // $(this).parent().prevAll().css("font-size","30px");
    // $(this).parent().siblings().css("font-size","30px");
    $('[href="#"]').dblclick(function(){
        alert("123");
    });
}

function fun4(){
    $("li:nth-child(n+2)").css("font-size","10px")
}