function fun1(){
    var str=document.getElementById("t1").value;
    var obj=document.getElementById("tt1");
    if(checkUser(str))
    {
        obj.innerHTML="用户名由英文字母和数字组成的4-16位字符，以字母开头。";
        return true;
    }
    else {
        obj.innerHTML="用户名格式错误。";
        return false;
    }
}

function checkUser(str){
    if(!((str[0]>='a'&&str[0]<='z'||str[0]>='A'&&str[0]<='Z')&&(str.length>=4&&str.length<=16)))
    return false;

    var i=0
    while(i<str.length){
        if(str[i]>='a'&&str[i]<='z'||str[i]>='A'&&str[i]<='Z'||str[i]>='0'&&str[i]<='9')
        i++;
        else break;
    }
    if(i<str.length)
    return false;

    return true;
}

function fun2(){
    var str=document.getElementById("t2").value;
    var obj=document.getElementById("tt2");
    if(checkPassword(str))
    {
        obj.innerHTML="密码由4-10位字符组成。";
        return true;
    }
    else {
        obj.innerHTML="密码格式错误。";
        return false;
    }
}

function checkPassword(str){
    if(str.length>=4&&str.length<=16)
    return true;
    else return false;
}

function fun3(){
    var str1=document.getElementById("t3").value;
    var str2=document.getElementById("t2").value;
    var obj=document.getElementById("tt3");
    if(checkConfirmPassword(str1,str2))
    {
        obj.innerHTML="密码和确认密码必须一致。";
        return true;
    }
    else {
        obj.innerHTML="与密码不一致或确认密码格式错误。";
        return false;
    }
}

function checkConfirmPassword(str1,str2){
    if(str1==str2)
    return true;
    else return false;
}

function checkAll(){
    if (fun1()&&fun2()&&fun3()) {
        document.getElementById("d1").style.display='none';
        document.getElementById("d2").style.display='block';
        fun4();
        setInterval(fun4,1000);
    } else {
        alert('输入格式有误');
    }
}

function fun4() {
    var data=new Date();
    var hour=data.getHours();
    var min=data.getMinutes();
    var sec=data.getSeconds();
    if(hour>=0&&hour<10)
    hour="0"+hour;
    if(min>=0&&min<10)
    min="0"+min;
    if(sec>=0&&sec<10)
    sec="0"+sec;
    var objs2=document.getElementById("time");
    objs2.innerHTML="当前时间："+hour + ":" + min + ":" + sec;
}


function fun5() {
    var btn=$("#btn");
    if(btn.text()=='更多'){
        $('#table2 tr:hidden').css("display","table-row");
        btn.text('收起');
    }else {
        $('#table2 tr:gt(3)').hide();
        btn.text('更多');
    }
}

function fun6() {
    document.getElementById("image").style.transform ='scale(1.5)';
}

function fun7() {
    document.getElementById("image").style.transform ='scale(1)';
}


var i=0;
function fun8(){
    var obj=document.getElementById("image");
    var arr=["img/01.jpg","img/02.jpg","img/03.jpg","img/04.jpg","img/05.jpg"];
    obj.src=arr[i++];
    if(i==arr.length){
        i=0;
    }
}
setInterval(fun8,3000);

function fun9(){
    var obj=document.getElementById("id-name");
    var x=Math.floor(Math.random()*256);
    var y=Math.floor(Math.random()*256);
    var z=Math.floor(Math.random()*256);
    obj.style.color="rgb("+x+","+y+","+z+")";
}
setInterval(fun9,500);