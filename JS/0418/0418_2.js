function student(name,sex,age,eng,com,math){
    this.name=name;
    this.sex=sex;
    this.age=age;
    this.eng=eng;
    this.com=com;
    this.math=math;
    this.display=function(){
        return this.name+" "+this.sex+" "+this.age+" "+this.eng+" "+this.com+" "+this.math;
    }
    this.allScore=function(){
        return parseInt(this.eng)+parseInt(this.com)+parseInt(this.math);
    }
    this.maxScore=function(){
        var temp=this.eng;
        if(temp<this.com)
            temp=this.com;
        if(temp<this.math)
            temp=this.math;
        return temp;
    }
}
function $(x){
    return document.getElementById(x);
}