function fun1()
{
    var data=new Date();
    var hour=data.getHours();
    var end=0;
    if(hour>=0&&hour<2)
    end=2;
    if(hour>=2&&hour<4)
    end=4;
    if(hour>=4&&hour<6)
    end=4;
    if(hour>=6&&hour<8)
    end=6;
    if(hour>=8&&hour<10)
    end=8;
    else if(hour>=10&&hour<12)
    end=10;
    else if(hour>=12&&hour<14)
    end=12;
    else if(hour>=14&&hour<16)
    end=14;
    if(hour>=16&&hour<18)
    end=16;
    if(hour>=20&&hour<22)
    end=20;
    if(hour>=22&&hour<24)
    end=22;
    document.getElementById("start").innerHTML=end+":"+"00";
    hour=end+2-1-hour;
    var min=60-1-data.getMinutes();
    var sec=60-data.getSeconds();
    hour="0"+hour;
    if(min>=0&&min<10)
    min="0"+min;
    if(sec>=0&&sec<10)
    sec="0"+sec;
    var objhour=document.getElementById("hour");
    var objmin=document.getElementById("min");
    var objsec=document.getElementById("sec");
    objhour.innerHTML=hour;
    objmin.innerHTML=min;
    objsec.innerHTML=sec;
    setInterval(fun1,1000);
}