function fun1(){
    var str=document.getElementById("t1").value;
    var obj=document.getElementById("tt1");
    if(checkUser(str))
    {
        obj.innerHTML="用户名由英文字母和数字组成的4-12位字符，并且用户名只能由字母、数字和下划线组成。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>用户名格式错误。";
        return false;
    }
}

function checkUser(str){
    if(!((str[0]>='a'&&str[0]<='z'||str[0]>='A'&&str[0]<='Z'||str[i]=='_')&&(str.length>=4&&str.length<=12)))
    return false;

    var i=0
    while(i<str.length){
        if(str[i]>='a'&&str[i]<='z'||str[i]>='A'&&str[i]<='Z'||str[i]>='0'&&str[i]<='9'||str[i]=='_')
        i++;
        else break;
    }
    if(i<str.length)
    return false;

    return true;
}


function fun2(){
    var str=document.getElementById("t2").value;
    var obj=document.getElementById("tt2");
    if(checkPassword(str))
    {
        obj.innerHTML="密码由6-12位字符组成。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>密码格式错误。";
        return false;
    }
}

function checkPassword(str){
    if(str.length>=6&&str.length<=12)
    return true;
    else return false;
}


function fun3(){
    var str1=document.getElementById("t3").value;
    var str2=document.getElementById("t2").value;
    var obj=document.getElementById("tt3");
    if(checkConfirmPassword(str1,str2))
    {
        obj.innerHTML="密码和确认密码必须一致。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>与密码不一致或确认密码格式错误。";
        return false;
    }
}

function checkConfirmPassword(str1,str2){
    if(str1==str2)
    return true;
    else return false;
}


function fun4(){
    var str=document.getElementById("t4").value; 
    var str=document.getElementById("t5").value; 
    var obj=document.getElementById("tt4");
    if(checkGender(str))
    {
        obj.innerHTML="必须选择性别。";
        return true;
    }
    else
    {
        obj.innerHTML="<img src='img/error.png'>没有选择性别。";
        return false;
    }
}

function checkGender(){
    var radios = document.getElementsByName("gender");
    var i=0;
    for(;i<radios.length;i++) 
    {
        if(radios[i].checked) 
        return true;
    }
    return false;
}


function fun5(){
    var str=document.getElementById("t6").value;
    var obj=document.getElementById("tt5");
    if(checkMail(str))
    {
        obj.innerHTML="电子邮箱信息必须包含@符号和.符号，且@符号不能在第1位，必须在.符号前面。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>电子邮箱格式错误。";
        return false;
    }
}

function checkMail(str){
    var pos=str.indexOf('@');
    var pos2=str.indexOf('.');
    if(pos==0||pos==-1||pos2==-1||pos>=pos2)
    return false;
    else return true;
}


function fun6(){
    var str=document.getElementById("t7").value;
    var obj=document.getElementById("tt6");
    if(checkTelephone(str))
    {
        obj.innerHTML="手机号码必须是11位数字，且由1开头。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>手机号码格式错误。";
        return false;
    }
}

function checkTelephone(str){
    if(str[0]!='1'||isNaN(str)||str.length!=11)
    return false;
    else return true;
}


function fun7(){
    var str=document.getElementById("t8").value;
    var obj=document.getElementById("tt7");
    if(checkDate(str))
    {
        obj.innerHTML="生日按“1985-05-01”格式输入，输入的年在1900-当前年份，输入的月在1-12之间，输入的日在1-31之间。";
        return true;
    }
    else {
        obj.innerHTML="<img src='img/error.png'>出生日期格式错误。";
        return false;
    }
}

function checkDate(str){
    var pos1=str.indexOf("-");
    var pos2=str.lastIndexOf("-");
    if(!(pos1==4&&pos2==7))
    return false;

    var arr=str.split("-");
    if(isNaN(arr[0])||isNaN(arr[1])||isNaN(arr[2]))
    return false;

    var nowdate=new Date();
    var nowyear=nowdate.getFullYear();
    if(arr[0]>nowyear||nowyear-arr[0]>124)
    return false;

    if(!(arr[1]>=1&&arr[1]<=12))
    return false;

    var temp;
    if(arr[1]==1||arr[1]==5||arr[1]==7||arr[1]==8||arr[1]==10||arr[1]==12)
    temp=31;
    if(arr[1]==4||arr[1]==6||arr[1]==9||arr[1]==11)
    temp=30;
    if(arr[0]%4==0&&arr[0]%100!=0||arr[0]%400==0){
        if(arr[1]==2)
        temp=29;
    }else if(arr[1]==2)
            temp=28;
    if(!(arr[2]>=1&&arr[2]<=temp))
    return false;

    return true;
}


function fun8(){
    var obj=document.getElementById("id-name");
    var x=Math.floor(Math.random()*256);
    var y=Math.floor(Math.random()*256);
    var z=Math.floor(Math.random()*256);
    obj.style.color="rgb("+x+","+y+","+z+")";
}


function fun9(){
    var image=document.getElementById("img");
    var opacity=Math.random();
    image.style.opacity=opacity;
}


function checkAll(){
    return fun1()&&fun2()&&fun3()&&fun4()&&fun5()&&fun6()&&fun7();
}


function clearForm(){
    var form=document.getElementById("form");
    var all=form.elements;
    var i=0;
    for(;i<all.length;i++)
    {
        if(!(all[i].type=='button'||all[i].type=='submit'))  
        all[i].value='';
    }
    
}    